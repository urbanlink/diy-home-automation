#include <Arduino.h>
#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include "MLX90615.h"
#include <I2cMaster.h>

#define SDA_PIN 4   //define the SDA pin
#define SCL_PIN 5   //define the SCL pin

SoftI2cMaster i2c(SDA_PIN, SCL_PIN);
MLX90615 mlx90615(DEVICE_ADDR, &i2c);

// Connect to the WiFi
const char* ssid = "TP-Link_6C08";
const char* password = "47457195";
const char* mqtt_server = "192.168.0.132";

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsgTime = 0;
long currentMsgTime = 0;
int msgDelayMillis = 1000*60*10;

char msg[50];
int value = 0;

// Initiate the wifi
// Try for max 30 sec
boolean setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  digitalWrite(LED_BUILTIN, LOW); delay(400);
  digitalWrite(LED_BUILTIN, HIGH); delay(400);
  digitalWrite(LED_BUILTIN, LOW); delay(400);
  digitalWrite(LED_BUILTIN, HIGH); delay(400);
  digitalWrite(LED_BUILTIN, LOW); delay(400);
  digitalWrite(LED_BUILTIN, HIGH);

  delay(1000);

  return true;

}

// Handle MQTT incoming data
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    // digitalWrite(LED_BUILTIN, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    // digitalWrite(LED_BUILTIN, HIGH);  // Turn the LED off by making the voltage HIGH
  }

}

// Reconnect to the MQTT server
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

// Send the temperature to the mqtt topic
boolean sendTemperature() {

  Serial.print("Ambient temperature: ");
  Serial.println(mlx90615.getTemperature(MLX90615_AMBIENT_TEMPERATURE));

  // sensors_event_t event;

  // dht.temperature().getEvent(&event);
  float t = mlx90615.getTemperature(MLX90615_AMBIENT_TEMPERATURE);
  dtostrf(t, 5, 2, msg);
  client.publish("sensor/temperature", msg);
  Serial.print("MQTT published: "); Serial.println(t);

  digitalWrite(LED_BUILTIN, LOW); delay(500);
  digitalWrite(LED_BUILTIN, HIGH);
  lastMsgTime = millis();
}

// SETUP
void setup() {

  pinMode(LED_BUILTIN, OUTPUT);     // Initialize the LED_BUILTIN pin as an output

  // Blink the built in led
  digitalWrite(LED_BUILTIN, HIGH); delay(100);
  digitalWrite(LED_BUILTIN, LOW); delay(100);
  digitalWrite(LED_BUILTIN, HIGH); delay(100);

  // Initialize serial communication
  Serial.begin(115200);

  // Make the wifi connection
  if (!setup_wifi()) {
    Serial.println("Error setting up wifi. abort");
    return;
  }

  // Initiate the MQTT server
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

}

// MAIN LOOP
void loop() {

  digitalWrite(LED_BUILTIN, HIGH);

  if (!client.connected()) { reconnect(); }
  client.loop();

  currentMsgTime = millis();

  if ((lastMsgTime == 0) || (currentMsgTime - lastMsgTime > msgDelayMillis)) {
    if (!sendTemperature())
      Serial.println("Error sending temperature");

  }
}
